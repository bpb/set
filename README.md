set
===

A `set` collection in JavaScript: store unique values of any type, whether
primitive values or object references.

This polyfill is compatible with the equals behavior and API of the [Set]
collection in ES6.


Supports:

- unique keys
- maintains insertion order
- ES6 `Set` key equivalency for NaN, -0, and +0
- stores primitives, objects (with JSON.stringify equivalency)
- Symbol.iterator for `for(value of myset)` iteration
- IE9+ (requires Object.defineProperty && Function.prototype.bind) but
  supports `for...of` iteration in supported environments

Description
-----------

`set` objects are collections of values, you can iterate its elements in
insertion order. A value in the set may only occur once; it is unique in the
set's collection.


Value equality
--------------

Because each value in the set has to be unique, the value equality will be
checked and is not based on the same algorithm as the one used in the ===
operator. Per the latest ECMAScript 6 specification, +0 and -0 are treated as
the same value in set objects. Also, NaN and undefined can also be stored in a set. NaN is considered the same as NaN (even though NaN !== NaN).

Properties
----------

### `size`

Returns the number of values in the set object.

Methods
-------

### `set.prototype.add(value)`

Appends a new element with the given value to the set object. Returns the set object.

### `set.prototype.clear()`

Removes all elements from the set object.

### `set.prototype.delete(value)`

Removes the element associated to the value and returns the value that set.prototype.has(value) would have previously returned. set.prototype.has(value) will return false afterwards.

### `set.prototype.entries()`

Returns a new Iterator object that contains an array of [value, value] for each element in the set object, in insertion order. This is kept similar to the Map object, so that each entry has the same value for its key and value here.

### `set.prototype.forEach(callbackFn[, thisArg])`

Calls callbackFn once for each value present in the set object, in insertion order. If a thisArg parameter is provided to forEach, it will be used as the this value for each callback.

### `set.prototype.has(value)`

Returns a boolean asserting whether an element is present with the given value in the set object or not.

### `set.prototype.keys()`

Is the same function as the values() function and returns a new Iterator object that contains the values for each element in the set object in insertion order.

### `set.prototype.values()`

Returns a new Iterator object that contains the values for each element in the set object in insertion order.

### `set.prototype[@@iterator]()`

Returns a new Iterator object that contains the values for each element in the set object in insertion order.


See also
--------

 - [Set]


[Set]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set