/* global describe it context */
var set = require('../set');
var assert = require('assert')

describe("set", function() {
    context('when in an ES6 environment', function() {
        describe("#values", function() {
            it("should return a new `iterable` object that contains the values for each of the elements in the set object in insertion order.", function() {
                var myset = new set([1, 2, 3])
                assert.deepEqual([...myset.values()], [1, 2, 3])
            })
        })
        describe("#keys", function() {
            it("should return a new `iterable` object that returns the element values in insertion order.", function() {
                var myset = new set([1, 2, 3])
                assert.deepEqual([...myset.keys()], [1, 2, 3])
            })
        })

        describe("#@@iterator", function() {
            it("should contain a Symbol.iterator property which iterates over the values in insertion order", function() {
                var myset = new set([1, 2, 3])
                assert.deepEqual([...myset], [1, 2, 3])
            })

        })
        describe("#entries", function() {
            it("should return a new `iterable` object that returns [value, value] in insertion order.", function() {
                var myset = new set([1, 2, 3])
                assert.deepEqual([...myset.entries()], [[1, 1], [2, 2], [3, 3]])
            })
        })
    })
})