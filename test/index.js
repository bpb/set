/* global describe it context */
var set = require('../set');
var arrayFrom = Array.from || function(iter) {
    var a = [];
    iter.forEach(function(el) {
        a.push(el)
    });
    return a
}
var expect = require('expect.js')
var assert = require('assert')



describe("set", function() {
    it("should be a (constructor) function", function() {
        expect(set).to.be.a('function')

        const myset = new set()
        expect(myset).to.be.a(set)
    })

    it("should throw a TypeError when called without `new`", function() {
        expect(function() {
            set()
        }).to.throwException(function(e) {
            expect(e).to.be.a(TypeError)
        })
    })

    /*
    it("should return a new `set` instance even when called without `new`", function() {
        const seta = set()
        expect(seta).to.be.a(set)
        const setb = set([1])
        expect(setb).to.be.a(set)
        expect(setb.has(1)).to.be(true)
    })
    */

    it("should have a `length` property whose value is `0`", function() {
        expect(set.length).to.be(0)
    })

    it("should add all the elements of `iterable` to itself if `iterable` is passed", function() {
        var myset = new set([1, 2, 3])
        expect(myset.size).to.be(3)
        expect(myset.has(1)).to.be(true)
        expect(myset.has(2)).to.be(true)
        expect(myset.has(3)).to.be(true)
    })

    it("should not contain any duplicate items", function() {
        var myset = new set([1, 2, 2, 3, 3, 3])
        expect(myset.size).to.be(3)

        var mysetb = new set()
        mysetb.add(1).add(2).add(2).add(3).add(3).add(3)
        expect(mysetb.size).to.be(3)
    })

    it("should maintain the insertion order of its elements", function() {
        assert.deepEqual(arrayFrom(new set([10, 9, 8, 7, 10])), [10, 9, 8, 7])
        assert.deepEqual(arrayFrom(new set([1, 2, 3, 4, 1])), [1, 2, 3, 4])
    })

    it("should treat multiple `NaN`s as duplicates", function() {
        var myset = new set()
        myset.add(NaN)
        myset.add(NaN)
        expect(myset.has(NaN)).to.be(true)
        expect(myset.size).to.be(1)

        var mysetb = new set([NaN, NaN])
        expect(mysetb.has(NaN)).to.be(true)
        expect(mysetb.size).to.be(1)
    })

    it("should treat -0 and +0 as equivalent", function() {
        var myset = new set([-0, +0])
        expect(myset.size).to.be(1)
    })

    it("should store `null` and `undefined`", function() {
        const myset = new set()
        myset.add(null)
        myset.add(null)
        myset.add(undefined)
        myset.add(undefined)
        expect(myset.size).to.be(2)

        const mysetb = new set([null, null, undefined, undefined, , , ])
        expect(mysetb.size).to.be(2)
    })

    it("should store strings", function() {
        const s = new set()
        s.add('hello')
        expect(s.has('hello')).to.be(true)
    })

    it("should store number primitives", function() {
        const s = new set()
        s.add(1)
        expect(s.has(1)).to.be(true)
    })

    it("should treat '1' and 1 as distinct items", function() {
        var myset = new set([1, '1'])
        expect(myset.size).to.be(2)
        expect(myset.has(1)).to.be(true)
        expect(myset.has('1')).to.be(true)
    })

    it("should store arrays", function() {
        var myset = new set()
        var array = [1, 2, 3]
        myset.add(array)
        expect(myset.size).to.be(1)
        expect(myset.has(array)).to.be(true)
    })

    it("should store objects", function() {
        var myset = new set()
        var obj = {
            id: 1
        }
        myset.add(obj)
        expect(myset.size).to.be(1)
        expect(myset.has(obj)).to.be(true)
    })

    it("should store functions", function() {
        var myset = new set()
        var identity = function identity(arg) {
            return arg
        }
        var noop = function noop() {}
        var empty = function empty() {}

        myset.add(identity).add(noop).add(empty)

        expect(myset.size).to.be(3)
        expect(myset.has(identity)).to.be(true)
        expect(myset.has(noop)).to.be(true)
        expect(myset.has(empty)).to.be(true)
    })

    it("should base array equivalency on `===` equality"
        /*, function() {
                var myset = new set()
                myset.add([1, 2, 3])
                myset.add([1, 2, 3])
                expect(myset.size).to.be(2)
                assert.deepEqual(arrayFrom(myset)[0], [1, 2, 3])
            }*/
    )

    it("should base object equivalency on `===` equality"
        /*, function() {
                const myset = new set()
                var joe = {
                    id: 1,
                    name: "joe",
                    hired: Date(2015, 0, 1)
                }
                myset.add(joe)
                expect(myset.has(joe)).to.be(true)
                expect(myset.has({
                    id: 1,
                    name: "joe",
                    hired: Date(2015, 0, 1)
                })).to.be(false)
            }*/
    )

    // These tests are different from the es6 specified behavior

    /*
    it("should base array equivalency on the array values", function() {
        var myset = new set()
        myset.add([1, 2, 3])
        myset.add([1, 2, 3])
        expect(myset.size).to.be(1)
        assert.deepEqual(myset.values()[0], [1, 2, 3])
    })

    it("should base object equivalency based on "key value" equivalency (aka `JSON.stringify() equivalency` ", function() {
        const myset = new set()
        myset.add({
            id: 1,
            name: "joe",
            hired: Date(2015, 0, 1)
        })
        expect(myset.has({
            id: 1,
            name: "joe",
            hired: Date(2015, 0, 1)
        })).to.be(true)
    })
    */

    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set/add
    describe("#add(entry)", function() {
        it("should add an element to the `set` instance", function() {
            var myset = new set()
            myset.add(1)
            expect(myset.size).to.be(1)
            myset.add("fancy")
            expect(myset.size).to.be(2)
        })

        it("should not add duplicate elements", function() {
            var myset = new set()
            myset.add(1)
            myset.add(1)
            expect(myset.size).to.be(1)
        })

        it("should return `this` to allow chaining", function() {
            var myset = new set()
            expect(function() {
                myset.add(1).add(2)
            }).to.not.throwException();
            expect(myset.size).to.be(2)
        })
    })

    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set/delete
    describe("#delete(entry)", function() {
        it("should remove the specified element from the `set` instance", function() {
            const myset = new set([1, 2])
            myset.delete(1)
            expect(myset.size).to.equal(1)
        })

        it("should return true when an element has been removed successfully from the `set` instance", function() {
            const myset = new set([1, 2])
            expect(myset.delete(1)).to.be(true)
        })

        it("should return false when an element is unsuccessfully removed from the `set` instance", function() {
            const myset = new set([1, 2])
            expect(myset.delete(3)).to.be(false)
        })

    })

    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set/has
    describe("#has(entry)", function() {
        context('when `set` contains `entry`', function() {
            it("should return true", function() {
                const myset = new set([1, 2])
                expect(myset.has(1)).to.be(true)
            })
        })
        context('when `set` does not contain `entry`', function() {
            it("should return `false`", function() {
                const myset = new set([1, 2])
                expect(myset.has(3)).to.be(false)
            })
        })
    })

    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set/clear
    describe("#clear()", function() {
        it("should remove all elements from a `set` instance", function() {
            const myset = new set([1, 2])
            myset.clear()
            expect(myset.size).to.be(0)
        })
    })

    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set/forEach
    describe("#forEach", function() {
        it("should execute a provided function once per each element in the `set`", function() {
            var count = 0,
                counter = function() {
                    count++
                }
            new set([1, 2, 3]).forEach(counter)
            expect(count).to.be(3)
        })

        it("should maintain the insertion order of the elements", function() {
            const originalArray = ["one", "two", "three", 1, 2, 3, '1', '2', '3']
            const myset = new set(originalArray)
            const setValues = []

            myset.forEach(function(value) {
                setValues.push(value);
            });

            assert.deepEqual(setValues, originalArray)
        })
        it("should accept a second parameter to use as the `this` binding", function() {
            var tracker = {
                    count: 0
                },
                counter = function() {
                    this.count++
                }

            new set([1, 2, 3]).forEach(counter, tracker)
            expect(tracker.count).to.be(3)
        })
    })

    describe("#size", function() {
        it("should return the number of elements in the `set` object", function() {
            const myset = new set([1, 2])
            expect(myset.size).to.equal(2)
        })
    })

    context("when in an environment that doesn't support Symbol.iterator", function() {
        describe("#values", function() {
            it("should return an array containing the values for each of the elements in the set", function() {
                var myset = new set([1, 2, 3])
                assert.deepEqual(myset.values(), [1, 2, 3])
            })
        })
        describe("#keys", function() {
            it("should return an array containing the values for each of the elements in the set in insertion order", function() {
                var myset = new set([1, 2, 3])
                assert.deepEqual(myset.keys(), [1, 2, 3])
            })
        })

        describe("#entries", function() {
            it("should return an array containing [value, value] for each of the elements in the set in insertion order", function() {
                if (!canIterate()) {
                    var myset = new set([1, 2, 3])
                    assert.deepEqual(myset.entries(), [[1, 1], [2, 2], [3, 3]])
                }
            })
        })
    })
})

function canIterate() {
    return typeof Symbol !== 'undefined' && 'iterator' in Symbol
}