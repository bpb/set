//
// Constructor
//

function set( /*iterable*/ ) {
    if (!(this instanceof set))
        throw new TypeError('must be constructed with \'new\' operator, e.g. \'new set()\'')

    // Don't enumerate _values object (also configurable && writeable === false)
    Object.defineProperty(this, '_values', {
        value: {},
        enumerable: false
    })

    var iterable = arguments[0]
    if (typeof iterable !== 'undefined') {
        if ('forEach' in iterable) {
            iterable.forEach(add, this)
        }
        else {
            throw new TypeError(iterable + 'is not an iterable object')
        }
    }
}

//
// Prototype
//

set.prototype = {
    add: add,
    has: has,
    'delete': remove, // delete is a reserved word
    clear: clear,
    forEach: forEach,
    values: valueIterator,
    keys: valueIterator,
    entries: entriesIterator
}

// size is a computed property
Object.defineProperty(
    set.prototype, 'size', {
        get: function get() {
            return objKeys(this._values).length
        }
    })

// Define an iterator property if we're in an ES6 environment
if (canIterate()) {
    set.prototype[Symbol.iterator] = valueIterator
}

//
// Prototype methods
//

function add(entry) {
    this._values[hashKey(entry)] = entry
    return this
}

function has(entry) {
    return hasOwnProperty.call(this._values, hashKey(entry))
}

function remove(entry) { // delete is a reserved word
    var k = hashKey(entry)
    var hasProp = hasOwnProperty.call(this._values, k)
    if (hasProp) {
        delete this._values[k]
    }
    return hasProp
}

function forEach(fn, thisArg) {
    for (var key in this._values) {
        if (hasOwnProperty.call(this._values, key)) {
            fn.call(thisArg, this._values[key])
        }
    }
}

function clear() {
    this.forEach(remove, this)
}

function entriesIterator() {
    var entries = []
    this.forEach(function(value) {
        entries.push([value, value])
    })

    // Return entries iterator if supported, else the entries array
    return canIterate() ? entries[Symbol.iterator]() : entries
}

function valueIterator() {
    var values = toArray.call(this)

    // Return values iterator if supported, else the values array
    return canIterate() ? values[Symbol.iterator]() : values
}

// Non standard, not added to prototype
function toArray() {
    var values = []
    forEach.call(this, values.push, values)
    return values
}

//
// Utilities
//

var objKeys = Object.keys || function objKeys(o) {
    var keys = []
    for (var key in o) {
        if (hasOwnProperty.call.call(o, key)) {
            keys.push(key)
        }
    }
}

function canIterate() {
    return (typeof Symbol) !== 'undefined' && 'iterator' in Symbol
}

function hashKey(obj) {
    return (typeof obj) + JSON.stringify(obj, replacer)
}

function replacer(key, value) {
    return (typeof value === 'function') ? value.toString() : value
}

module.exports = set